#!/usr/bin/bash

if rpm -q --quiet libtracecmd; then
    :
else
    sudo dnf install -y libtracecmd
    if [[ $? != 0 ]]; then
       echo "install of libtracecmd failed!"
       exit 1
    fi
fi

echo "The libtracecmd is meant to be used by trace-cmd. Check installation."
if [[ ! -f /usr/lib64/libtracecmd.so.1 ]]; then
    echo "/usr/lib64/libtracecmd.so.1 not found!"
    exit 2
fi

echo "Check the trace-cmd works."
if ! rpm -q --quiet trace-cmd; then
    sudo dnf install -y trace-cmd
    if [[ $? != 0 ]]; then
        echo "install trace-cmd failed when libtracecmd exist!"
        exit 3
    fi
fi
trace-cmd list || exit 4

exit 0
